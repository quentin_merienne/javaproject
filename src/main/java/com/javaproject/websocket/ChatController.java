package com.javaproject.websocket;

import com.javaproject.myapp.config.WebSocketConfigSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by QT on 23/06/2016.
 */
@Controller
public class ChatController {
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private StockSession stockSession;

    @SubscribeMapping
    public List<String> getActiveUsers(){
        return stockSession.recoverAllSession();
    }
}
