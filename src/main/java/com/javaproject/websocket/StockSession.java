package com.javaproject.websocket;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by QT on 23/06/2016.
 */
public class StockSession {

    private Map<String,String> activeSessions = new ConcurrentHashMap<>();

    public void addSession(String id, String userName){
        this.activeSessions.put(id,userName);
    }

    public String recoverSession(String id){
        return this.activeSessions.get(id);
    }

    public void deleteSession(String id){
        this.activeSessions.remove(id);
    }

    public List<String> recoverAllSession(){
        List<String> s = this.activeSessions.keySet().stream().map(key -> this.activeSessions.get(key)).collect(Collectors.toList());
        return s;
    }
}
