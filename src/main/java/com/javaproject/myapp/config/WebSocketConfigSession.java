package com.javaproject.myapp.config;

import com.javaproject.websocket.StockSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;

import javax.annotation.Resource;
import javax.inject.Inject;
import java.util.Optional;

/**
 * Created by QT on 23/06/2016.
 */
@Configuration
public class WebSocketConfigSession {

    private static final String LOGIN_DESTINATION = "/topic/online/connect";// connect
    private static final String LOGOUT_DESTINATION = "/topic/online/disconnect"; // disconnect

    @Bean
    public StockSession getInstanceSession(){
        return new StockSession();
    }

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @EventListener
    private void handleSessionDisconnect(SessionDisconnectEvent event){
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String username = headers.getUser().getName();
        Optional.ofNullable(getInstanceSession().recoverSession(event.getSessionId()))
            .ifPresent(login -> {
                simpMessagingTemplate.convertAndSend(LOGOUT_DESTINATION, username);
                getInstanceSession().deleteSession(event.getSessionId());
            });
    }

    @EventListener
    private void handleSessionConnected(SessionConnectedEvent event){
        SimpMessageHeaderAccessor headers = SimpMessageHeaderAccessor.wrap(event.getMessage());
        String username = headers.getUser().getName();

        simpMessagingTemplate.convertAndSend(LOGIN_DESTINATION, username);

        // We store the session as we need to be idempotent in the disconnect event processing
        getInstanceSession().addSession(headers.getSessionId(), username);
    }


}
