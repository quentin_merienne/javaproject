/**
 * Data Transfer Objects used by Spring MVC REST controllers.
 */
package com.javaproject.myapp.web.rest.dto;
