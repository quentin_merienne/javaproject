/**
 * Data Access Objects used by WebSocket services.
 */
package com.javaproject.myapp.web.websocket.dto;
