package com.javaproject.business.ws;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * Created by nmendouz on 21/06/2016.
 */
public class SearchAPIGitHub {

    public static void main(String[] args) {

        SearchAPIGitHub tets = new SearchAPIGitHub();
        tets.run();
    }

    //Déclaration de l'URL dans url

    String url = "https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc";

    public void run() {
        try {

            String resultatJson = IOUtils.toString(new URL(url));
           // System.out.println(resultatJson);

            JsonElement jelement = new JsonParser().parse(resultatJson);

            JsonObject jobject = jelement.getAsJsonObject(); // recupérer les données a partir du premier { données complètes
            JsonArray items = jobject.getAsJsonArray("items"); // On se positionne sur l'items collection [] d'items c'est un array


            for (JsonElement element : items){
              //  System.out.println(element.toString());
                JsonObject myItem = element.getAsJsonObject();

                String name = myItem.get("name").toString();
                String description = myItem.get("description").toString();
                String link = myItem.get("html_url").toString();
                System.out.println(name);
                System.out.println(description);
                System.out.println(link);

            }

     } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}




/* ******************************************************************************* JACKSON
            // Parsage du JSON dans les objets InformationsGitHub & ReposGitHub

            ObjectMapper mapperGitHub = new ObjectMapper();
            mapperGitHub.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            RepoGitHub infos = mapperGitHub.readValue(resultatJson, RepoGitHub.class);

            //parcour des éléments de repoGitHub(Jackson)

            for (InformationsGitHub info : infos.getItems()) {

                System.out.println(info.getName());
                System.out.println(info.getDescription());
                System.out.println(info.getHtml_url());
                //Code à exécuter dans la boucle
            }


            System.out.println("obj =" + infos.getItems());

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
*/
