/**
 * Created by QT on 22/06/2016.
 */

package com.javaproject.business.ws;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.commons.io.IOUtils;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.GZIPInputStream;

public class GsonSearchAPIStackOverFlow {
    public static void main(String[] args) {
        GsonSearchAPIStackOverFlow api1 = new GsonSearchAPIStackOverFlow();
        api1.jsonToJava("javascript");
    }

    public List<ResultAPI> jsonToJava(String search){
        List<ResultAPI> result = new ArrayList<>();
        ResultAPI resultAPI = new ResultAPI();
        try {
            String url = "http://api.stackexchange.com/2.2/questions?order=desc&site=stackoverflow&search="+search;
            // Convert JSON string to Object
            URL urlDemo = new URL(url);
            URLConnection yc = urlDemo.openConnection();
            yc.setDoOutput(true);
            BufferedReader resultJson = new BufferedReader (new InputStreamReader(
                (new GZIPInputStream(yc.getInputStream () )) ));
            String resultatJson = IOUtils.toString(resultJson);

            JsonElement jElement = new JsonParser().parse(resultatJson);
            JsonObject jObject = jElement.getAsJsonObject();
            JsonArray items = jObject.getAsJsonArray("items");

            for (JsonElement e:items
                 ) {
                JsonObject item = e.getAsJsonObject();
                resultAPI.setDescription(item.get("tags").toString());
                resultAPI.setTitle(item.get("title").toString());
                resultAPI.setUrl(item.get("link").toString());
                //System.out.println(resultAPI.getDescription()+resultAPI.getUrl()+resultAPI.getTitle());
                result.add(resultAPI);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}
