/**
 * Created by QT on 21/06/2016.
 */
(function() {
    'use strict';

    angular
        .module('javaProjectApp')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {
        $stateProvider.state('search', {
            parent: 'app',
            url: '/search-test',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/search/search.html',
                    controller: 'searchController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
