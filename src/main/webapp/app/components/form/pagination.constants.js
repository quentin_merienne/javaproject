(function() {
    'use strict';

    angular
        .module('javaProjectApp')
        .constant('paginationConstants', {
            'itemsPerPage': 20
        });
})();
