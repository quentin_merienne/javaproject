/**
 * Created by QT on 23/06/2016.
 */
(function() {
    'use strict';

    angular
        .module('javaProjectApp')
        .config(myStateConfig);

    myStateConfig.$inject = ['$stateProvider'];

    function myStateConfig($stateProvider) {
        $stateProvider.state('tchat', {
            parent: 'app',
            url: '/tchat',
            data: {
                authorities: []
            },
            views: {
                'content@': {
                    templateUrl: 'app/tchat/tchat.html',
                    controller: 'TchatController',
                    controllerAs: 'vm'
                }
            },
            resolve: {
                mainTranslatePartialLoader: ['$translate', '$translatePartialLoader', function ($translate,$translatePartialLoader) {
                    $translatePartialLoader.addPart('global');
                    return $translate.refresh();
                }]
            }
        });
    }
})();
